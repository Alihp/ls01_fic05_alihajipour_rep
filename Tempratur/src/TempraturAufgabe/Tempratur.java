package TempraturAufgabe;

public class Tempratur {
	// Ali Hajipour

	public static void main(String[] args) {
		String s1="Fahrenheit";
		String s2="Celcius";
		System.out.printf("%-11s|%10s\n",s1,s2);			
		System.out.printf("-----------------------\n");
		System.out.printf("%-11d|%10.2f\n",-20,-28.8889);
		System.out.printf("%-11d|%10.2f\n",-10,-23.3333);
		System.out.printf("%s%-10d|%10.2f\n","+",0,-17.7778);
		System.out.printf("%s%-10d|%10.2f\n","+",20,-6.6667);
		System.out.printf("%s%-10d|%10.2f\n","+",30,-1.1111);
	}

}
